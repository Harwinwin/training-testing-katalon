<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Token_Fiona_FIF</name>
   <tag></tag>
   <elementGuidId>f5502309-8b7c-41aa-8743-d3c444ab3cdd</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;userId\&quot;: \&quot;${user}\&quot;,\n    \&quot;password\&quot;: \&quot;${password}\&quot;\n}\n&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <katalonVersion>9.2.0</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://fifada-qa-lb01.fifgroup.co.id/backend/user/login</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'fiona'</defaultValue>
      <description></description>
      <id>649455dd-81f1-4bb4-bf1f-cd66b74be6d0</id>
      <masked>false</masked>
      <name>user</name>
   </variables>
   <variables>
      <defaultValue>'zQe9Rk4qqvzs3Xyz'</defaultValue>
      <description></description>
      <id>2abbb9f2-f612-4fc6-8b9e-2e729a546f56</id>
      <masked>false</masked>
      <name>password</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

GlobalVariable.GetToken = WS.getElementPropertyValue(response, 'result.accessToken')
System.out.println(GlobalVariable.GetToken)

//WS.verifyElementPropertyValue(response, 'result.accessToken', &quot;eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJ1c2VySWRcIjpcImZpb25hXCIsXCJ0eXBlXCI6XCJCQUNLRU5EQURNSU5cIixcImJhY2tlbmRVc2VySWRcIjpcImZpb25hXCJ9IiwianRpIjoiZDJiZTBjMDYtYmU4OC00NGMzLTk0NWYtZjAzZDQ4NzcwNjk0IiwiaWF0IjoxNzA2MDcxMDI5LCJleHAiOjE3MDYwNzQ2Mjl9.pr-HXPb7gKrqR1BiGDArcTzZ8lq_YU6c9itnWNTcL2nLAoRHhoq3lrHPJAbjyEsEkXy82qAR3xdL57baVO9FCw&quot;)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
